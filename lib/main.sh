#!/usr/bin/env bash

main()
{
    clear
    
    printf "Szia, mit szeretnél?\n\n"
    printf "1. Kiadás\n"
    printf "2. Bevétel\n"
    printf "3. Listázás\n"
    printf "4. Projekt beállítása\n"
    printf "5. Adatbázis karbantartása\n"
    printf "q. Kilépés\n\n"

    printf $CYAN
    read subprogram
    printf $NOCOLOR

    case $subprogram in
	1)
	    . lib/cost.sh
	    ;;
	2)
	    . lib/income.sh
	    ;;
	3)
	    . lib/lists.sh
	    show_list_menu
	    ;;
	4)
	    . lib/project.sh
	    ;;
	5)
	    . lib/database.sh
	    ;;
	q)
	    exit 0
	    ;;
	*)
	    main
	    ;;
	esac
}
