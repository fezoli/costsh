#!/usr/bin/env bash

monthly_report()
{
    clear

    formatted_month=`date +%Y.\ %B`

    printf $BLUE
    printf "E HAVI KIADÁSOK $formatted_month\n\n"
    printf $NOCOLOR

    this_month=`date +%Y%m`'00'
    sql="SELECT movement.id, movement.name, movement.amount, category.name, movement.date, movement.time FROM movement INNER JOIN category ON movement.category = category.id  WHERE date > $this_month ORDER BY movement.date, movement.time"
    db $sql

    echo
    read
    main
}

monthly_report_by_category()
{
    clear

    formatted_month=`date +%Y.\ %B`
    
    printf $BLUE
    printf "E HAVI KIADÁSOK KATEGÓRIÁNKÉNT $formatted_month\n\n"
    printf $NOCOLOR

    this_month=`date +%Y%m`'00'
    sql="SELECT sum(movement.amount), category.name FROM movement INNER JOIN category ON movement.category = category.id  WHERE date > $this_month GROUP BY category.id ORDER BY category.name"
    db $sql

    echo
    read
    main
}


show_list_menu()
{
    clear

    printf $BLUE
    printf "LISTÁK\n\n"
    printf $NOCOLOR
    
    printf "1. Havi kiadások\n"
    printf "2. Havi kiadások kategóriánként\n"
    printf "3. Havi büdzsé állapota\n"

    printf $CYAN
    read subprogram
    printf $NOCOLOR

    case $subprogram in
	1)
	    monthly_report
	    ;;
	2)
	    monthly_report_by_category
	    ;;
	3)
	    clear
	    print_budget_summary
	    read
	    ;;
	*)
	    main
	    ;;
	esac
}
