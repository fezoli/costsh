#!/usr/bin/env bash

clear

printf $BLUE
printf "KIADÁS RÖGZÍTÉSE\n\n"
printf $NOCOLOR

printf "Megnevezés: $CYAN"
read NAME
printf $NOCOLOR

printf "Összeg: $CYAN"
read AMOUNT
AMOUNT=`expr 0 - $AMOUNT`
printf $NOCOLOR

sql="SELECT id, name FROM category"
categories=`db $sql`
echo $categories
printf "Kategória: $CYAN"
read CATEGORY
printf $NOCOLOR

dt=`date +%Y%m%d`
printf "Dátum (%s): $CYAN" $dt
read DATE
printf $NOCOLOR
if [ -z $DATE ]; then
   DATE=$dt
fi

tm=`date +%H%M`
printf "Idő (%s): $CYAN" $tm
read TIME
printf $NOCOLOR
if [ -z $TIME ]; then
   TIME=$tm
fi

sql="INSERT INTO movement (name, amount, category, date, time) VALUES ('$NAME', $AMOUNT, $CATEGORY, $DATE, $TIME)"
db $sql

print_budget_summary

read

return 0
