#!/usr/bin/env bash

LC_ALL=hu_HU.UTF-8

MYDIR=$HOME/.local/costsh

if [ ! -d "$MYDIR" ]; then
    mkdir -p $MYDIR
fi

DB=$MYDIR/costsh.db

##### DEFAULT PROJECT #####
DEFAULT_PROJECT=`date +%Y%m`
