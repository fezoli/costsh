#!/usr/bin/env bash

print_budget_summary()
{
    sql="SELECT budget FROM project WHERE name='$DEFAULT_PROJECT'"
    budget=`db $sql`

    month=`date +%m`
    monthstart=`date +%Y%m`'00'

echo $monthstart
    
    sql="SELECT sum(amount) FROM movement where date > $monthstart AND amount > 0" 
    sum=`db $sql`
    printf "\nE havi összes bevétel: $RED%d$NOCOLOR Ft\n" $sum
    sql="SELECT -1*sum(amount) FROM movement where date > $monthstart AND amount < 0" 


    sum=`db $sql`
    printf "\nE havi összes kiadás: $RED%d$NOCOLOR Ft\n" $sum
    printf "Teljes büdzsé: $GREEN%d$NOCOLOR Ft\n" $budget
    
    today=`date +%d`
    last_day=`date -d "$month/1 + 1 month - 1 day" "+%d"`
    days_left=`expr $last_day - $today`
    budget_left=`expr $budget - $sum`
    printf "Még elkölthető a következő %d napban: $YELLOW%d$NOCOLOR Ft\n\n" $days_left $budget_left
}
